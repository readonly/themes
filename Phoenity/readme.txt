=================
Phoenity for Sylpheed Claws
=================

This theme has been tested using Sylpheed-Claws for Win32 on my Win98SE.
I wonder if this theme could work in other 'claws'. FYI, this theme is
*complete* with icons replacing all the internal default icons.

Any feedback would be appreciated.

=================
Lim Chee Aun
http://phoenity.com/
=================