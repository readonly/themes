Theme name: Hydroxygen
Version: 0.9 (September 7, 2009)
Authors:
    Salvatore De Paolis <iwkse@claws-mail.org>
        

This package contains a Claws Mail icon theme, tested agains Claws version
3.4.0 and later. This theme is made up entirely of PNG icons (the second Claws
theme to do this, to my knowledge.)

YOU NEED CLAWS MAIL VERSION 3.4.0 OR LATER IN ORDER TO BE ABLE TO USE THIS.

The icons in this package are for the most an adaptation for the Claws Mail project 
of the computer icons released by the Hydroxygen Project . Most of the icons in this package 
are original Hydroxygen icons, some of them are instead created by me combining or modifying
what already existed.

This package is released in the public domain. See the file LICENSE.txt.

END OF FILE
