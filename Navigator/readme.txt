
"Navigator" - adapted and improved theme for Claws Mail. Simulates the "classic" theme used by Netscape® Communicator, Mozilla® and SeaMonkey®. Contains a few changes due to special demands of Claws Mail. 

The contents of this directory are subject to the Mozilla Public License Version 1.1; you may not use this directory except in compliance with the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/

Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for the specific language governing rights and limitations under the License.

The Original Code is by The SeaMonkey® Project. 
Contributor: Daniel Schneider, linux@knetfeder.de