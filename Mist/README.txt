Theme name: Mist, based on Gnome.
This README, except for these first lines, is the original from the Gnome theme.
Version: 1.0 (August 8, 2009)
Authors:
    Ciprian Popovici <http://xlife.zuavra.net/index.php/tag/claws-mail/>
    The Tango Project <http://tango.freedesktop.org/>
    Elisabeth Getzner <meckermaus@gmx.at>

This package contains a PNG Claws Mail icon theme, tested agains Claws version
3.7.0 and later. 

YOU NEED CLAWS MAIL VERSION 3.7.0 OR LATER IN ORDER TO BE ABLE TO USE THIS.
(Since it only recently startet to support transparency in .png's. Older 
versions may only display completely opaque icons.)

The icons in this package are an adaptation for the Claws Mail project of
the default Gnome icons installed on debian-based linux systems like ubuntu.
The icon-set was originally based on the Tango Icon Theme of Ciprian Popovici, 
as mentioned above. 

All icons are published under the GPL, therefore open to modify.

All the icons in this package are original Gnome icons, original works
authored by me, or combinations of several Tango icons and/or original bits
by me.

This theme was created in an attempt to emulate the looks of Novell's Evolution (TM)
and to create a better integration of Claws-Mail into the ubuntu/debian desktop.

END OF FILE
