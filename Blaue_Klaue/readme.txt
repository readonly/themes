
"Blaue Klaue" (German; blue claw) 0.3 - theme for Claws Mail 3.x
Created by Daniel Schneider in February 2008

This theme is licenced under GPL v2 and Creative Commons BY-ND-SA.

linux@knetfeder.de
http://www.knetfeder.de/linux/index.php?entry=entry080204-163111
