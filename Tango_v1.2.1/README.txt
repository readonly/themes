Theme name: Tango
Version: 1.2.1 (September 6, 2009)
Authors:
    Ciprian Popovici <http://xlife.zuavra.net/index.php/tag/claws-mail/>
    The Tango Project <http://tango.freedesktop.org/>

This package contains a Claws Mail icon theme, tested agains Claws version
3.4.0 and later. This theme is made up entirely of PNG icons (the first Claws
theme to do this, to my knowledge.)

YOU NEED CLAWS MAIL VERSION 3.4.0 OR LATER IN ORDER TO BE ABLE TO USE THIS.

The icons in this package are an adaptation for the Claws Mail project of
the computer icons released by the Tango Project as version 0.8.90. All the
icons in this package are original Tango icons, original works authored by
me, or combinations of several Tango icons and/or original bits by me.

This package is released in the public domain. See the file LICENSE.txt.

The package contains two icons that vaguely resemble the Adobe PDF and PS
logos (mime_pdf.png and mime_ps.png, respectively). They were created from
scratch, not obtained by editing the original logos. It is not my intention
to infringe upon any trademarks owned by Adobe nor to create any confusion
regarding their products, but simply to provide means of loosely identifying
certain types of mail attachments.

END OF FILE
